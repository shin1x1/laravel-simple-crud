<?php
return [
    'routing' => [
        'prefix' => '/crud'
    ],

    'view' => [
        'parent_view' => 'simple-crud::base',
        'prefix' => 'simple-crud::',
    ],
];