<?php
namespace Shin1x1\LaravelSimpleCrud\Schema;

/**
 * Class SchemaLabel
 * @package Shin1x1\LaravelSimpleCrud\Schema
 */
class SchemaLabel extends AbstractSchema
{
    /**
     * @return bool
     */
    public function isLabel()
    {
        return true;
    }
}
