<?php
namespace Shin1x1\LaravelSimpleCrud\Schema;

/**
 * Class SchemaSelect
 * @package Shin1x1\LaravelSimpleCrud\Schema
 */
class SchemaSelect extends AbstractSchema
{
    /**
     * @return bool
     */
    public function isSelect()
    {
        return true;
    }
}
